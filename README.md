# L Line Analysis
 
 The repository contains the solution exploration of train data from the Chicago L - A train line that runs through the city.

 Data was sourced [here](https://data.cityofchicago.org/Transportation/CTA-Ridership-L-Station-Entries-Daily-Totals/5neh-572f).

 Further information about the train line can be found [here](https://en.wikipedia.org/wiki/Chicago_%22L%22)

# High Level Components

* Numercial Data
* Seasonality
* Transportation
* Data Analysis
* Mixed Integer/Linear Programming
* Product Vision
* Customer Use Case Analysis

## Project structure

~~~~
|- l_line
	|- data
	|	|- external
	|	|- interim
	|	|- processed
	|	|- raw
	|- docs
	|- models
	|- notebooks # to view in browser add the "Bitbucket Notebook Viewer" to your "Integrations"
	|- references
	|- reports
	|	|- figures
	|- requirements.txt
	|- setup.py
	|- l_line
		|- __init__.py
		|- data
		|- models
		|- visualization
~~~~ 

## How To

~~~
git clone https://bitbucket.org/intrinsy/l_line

cd your/repo/location/l_line

mkvirtualenv l_line

pip install -r requirements.txt

# run notebooks

jupyter notebook

# there are currently two notebooks
# 1. ./notebooks/data_analysis.ipynb
# 2. ./reports/business_analysis.ipynb
~~~

### MIP/LP Solver

* GLPK - https://www.gnu.org/software/glpk/
* GLPK - "brew install glpk" or [install](http://hichenwang.blogspot.com/2011/08/fw-installing-glpk-on-mac.html)
* [pulp](https://pythonhosted.org/PuLP/) support multiple solvers
* Commercially CPLEX or Gurobi is more suitable
* [Benchmarks](http://miplib.zib.de/)

