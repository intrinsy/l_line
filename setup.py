from setuptools import setup, find_packages

VERSION = '0.0.1'

requirements = [
    "jupyter==5.4.0",
    "matplotlib==2.1.1",
    "pandas==0.22.0",
    "PuLP==1.6.8"
]

setup(
    name='l_lin',
    version=VERSION,
    packages=find_packages(exclude=['tests']),
    description='Chicago L line analysis',
    author='Fabion Kauker',
    author_email='f.kauker@gmail.com',
    install_requires=requirements,
)
